var gulp = require('gulp');
var ngHtml2Js = require("gulp-ng-html2js");
var concat = require('gulp-concat');
var rimraf = require('rimraf');
var minifyHTML = require('gulp-minify-html');
var uglify = require('gulp-uglify');
var rename = require("gulp-rename");

var minifyHtmlOpts = {
    empty: true,
    spare: true,
    quotes: true
};
var html2jsOpts = {
    moduleName: "ionic-datepicker.templates",
    prefix: "ionic-datepicker/"
};

gulp.task('template', ['clean-dist'], function () {
    return gulp.src('./src/*.html')
        .pipe(minifyHTML(minifyHtmlOpts))
        .pipe(ngHtml2Js(html2jsOpts))
        .pipe(concat('templates.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dist/'));
});

gulp.task('clean-dist', function (cb) {
    rimraf('./dist', cb);
});

gulp.task('pre-build', ['clean-dist', 'template'], function () {
    return gulp.src('./src/ionic-datepicker.js')
        .pipe(gulp.dest('./dist'))
        .pipe(uglify())
        .pipe(rename("ionic-datepicker.min.js"))
        .pipe(gulp.dest('./dist'));
});

gulp.task('build', ['pre-build'], function () {
    return gulp.src(['./dist/templates.js', './dist/ionic-datepicker.min.js'])
        .pipe(concat('ionic-datepicker.all.js'))
        .pipe(gulp.dest('./dist'));
});

gulp.task('default', ['build']);