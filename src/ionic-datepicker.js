"use strict";
var module = angular.module("ionic-datepicker", ['ionic', 'ionic-datepicker.templates']);

module.directive("ionicDatepicker", ["$ionicPopup", function ($ionicPopup) {
    //var script = document.querySelector("script[src$='ionic-datepicker.js']");
    //var currentScriptPath = script.src;

    return {
        restrict: 'E',
        scope: {
            title: '@',
            monthText: '@',
            dateText: '@',
            yearText: '@',
            setText: '@'
        },
        template: '<span class="input-label">{{ title }}</span>' +
        '<input type="text" placeholder="{{ title }}" ng-keydown="$event.preventDefault();" ng-model="textRepresentation">',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelController) {
            // Prevent getting exception when no ngModel supplied
            if(ngModelController == null) {
                ngModelController = {
                    '$setViewValue': function() {}
                };
            }
            var now = new Date();
            scope.inputs = {
                month: now.getMonth() + 1,
                day: now.getDate(),
                year: now.getFullYear()
            };
            scope.textRepresentation = '';

            scope.checkForLeadingZero = function () {
                scope.inputs.month = Number(scope.inputs.month) < 10 ? '0' + Number(scope.inputs.month) : scope.inputs.month;
                scope.inputs.day = Number(scope.inputs.day) < 10 ? '0' + Number(scope.inputs.day) : scope.inputs.day;

                if(scope.popup)
                    scope.textRepresentation = scope.inputs.month + ' - ' + scope.inputs.day + ' - ' + scope.inputs.year;
            };
            scope.checkForLeadingZero();

            var preventWatch = false;
            scope.increaseDays = function () {
                var day = Number(scope.inputs.day),
                    d = angular.copy(now);
                preventWatch = true;

                d.setDate(day + 1);
                if (d.getMonth() == Number(scope.inputs.month) - 1) {
                    day++;
                    now.setDate(day);
                    ngModelController.$setViewValue(now);
                }

                scope.inputs.day = day;
                scope.checkForLeadingZero();
            };
            scope.decreaseDays = function () {
                var day = Number(scope.inputs.day),
                    d = angular.copy(now);
                preventWatch = true;

                d.setDate(day - 1);
                if (d.getMonth() == Number(scope.inputs.month) - 1) {
                    day--;
                    now.setDate(day);
                    ngModelController.$setViewValue(now);
                }

                scope.inputs.day = day;
                scope.checkForLeadingZero();
            };
            scope.increaseMonth = function () {
                var month = Number(scope.inputs.month),
                    d = angular.copy(now);
                preventWatch = true;

                // Zero - indexed so the month value is actually always + 1
                d.setMonth(month);
                if (d.getFullYear() == Number(scope.inputs.year)) {
                    month++;
                    now.setMonth(month - 1);
                    ngModelController.$setViewValue(now);

                    if (now.getDate() != Number(scope.inputs.day)) {
                        now.setDate(scope.inputs.day = 1);
                        now.setMonth(month - 1);
                        ngModelController.$setViewValue(now);
                    }
                }

                scope.inputs.month = month;
                scope.checkForLeadingZero();
            };
            scope.decreaseMonth = function () {
                var month = Number(scope.inputs.month),
                    d = angular.copy(now);
                preventWatch = true;

                // Zero - indexed the month value is actually always + 1 so we will be subtract 2
                d.setMonth(month - 2);
                if (d.getFullYear() == Number(scope.inputs.year)) {
                    month--;
                    now.setMonth(month - 1);
                    ngModelController.$setViewValue(now);

                    if (now.getDate() != Number(scope.inputs.day)) {
                        d.setDate(0);
                        now.setDate(scope.inputs.day = d.getDate());
                        now.setMonth(month - 1);
                        ngModelController.$setViewValue(now);
                    }
                }

                scope.inputs.month = month;
                scope.checkForLeadingZero();
            };
            scope.increaseYear = function () {
                var year = Number(scope.inputs.year),
                    d = angular.copy(now);
                preventWatch = true;

                year++;
                d.setFullYear(year);
                d.setMonth(now.getMonth() + 1);
                d.setDate(0);
                now.setFullYear(year);
                ngModelController.$setViewValue(now);
                if (now.getDate() != Number(scope.inputs.day)) {
                    now.setMonth(Number(scope.inputs.month) - 1);
                    now.setDate(scope.inputs.day = 1);
                    ngModelController.$setViewValue(now);
                }

                scope.inputs.year = year;
                scope.checkForLeadingZero();
            };
            scope.decreaseYear = function () {
                var year = Number(scope.inputs.year),
                    d = angular.copy(now);
                preventWatch = true;

                year--;
                d.setFullYear(year);
                d.setMonth(now.getMonth() + 1);
                d.setDate(0);
                now.setFullYear(year);
                ngModelController.$setViewValue(now);
                if (now.getDate() != Number(scope.inputs.day)) {
                    now.setMonth(Number(scope.inputs.month) - 1);
                    now.setDate(scope.inputs.day = d.getDate());
                    ngModelController.$setViewValue(now);
                }

                scope.inputs.year = year;
                scope.checkForLeadingZero();
            };
            scope.$watch('inputs.year', function (newValue, oldValue) {
                if (preventWatch) {
                    preventWatch = !preventWatch;
                    return;
                }
                if (newValue == oldValue)
                    return;
                if (!!isNaN(newValue)) {
                    scope.inputs.year = oldValue;
                    return;
                }
            });
            scope.$watch('inputs.month', function (newValue, oldValue) {
                if (preventWatch) {
                    preventWatch = !preventWatch;
                    return;
                }
                if (newValue == oldValue)
                    return;
                if (!!isNaN(newValue)) {
                    scope.inputs.month = oldValue;
                    return;
                }
            });
            scope.$watch('inputs.day', function (newValue, oldValue) {
                if (preventWatch) {
                    preventWatch = !preventWatch;
                    return;
                }
                if (newValue == oldValue)
                    return;
                if (!!isNaN(newValue)) {
                    scope.inputs.day = oldValue;
                    return;
                }
            });
            scope.monthBlur = function (e) {
                if (scope.inputs.month > 12) {
                    scope.inputs.month = 12;
                } else if (scope.inputs.month < 1) {
                    scope.inputs.month = 1;
                }
                now.setMonth(scope.inputs.month - 1);
                ngModelController.$setViewValue(now);
                scope.checkForLeadingZero();
            };
            scope.dayBlur = function (e) {
                var d = angular.copy(now);
                d.setMonth(d.getMonth() + 1);
                d.setDate(0);
                if (scope.inputs.day > d.getDate()) {
                    scope.inputs.day = d.getDate();
                } else if (scope.inputs.day < 0) {
                    scope.inputs.day = 1;
                }
                now.setDate(scope.inputs.day);
                ngModelController.$setViewValue(now);
                scope.checkForLeadingZero();
            };
            scope.yearBlur = function (e) {
                var inc = now.getFullYear() < Number(scope.inputs.year);
                now.setFullYear(scope.inputs.year);
                ngModelController.$setViewValue(now);
                if (inc) {
                    if (now.getDate() != Number(scope.inputs.day)) {
                        now.setMonth(Number(scope.inputs.month) - 1);
                        now.setDate(scope.inputs.day = 1);
                        ngModelController.$setViewValue(now);
                    }
                } else {
                    var d = angular.copy(now);
                    d.setDate(0);
                    if (now.getDate() != Number(scope.inputs.day)) {
                        now.setMonth(Number(scope.inputs.month) - 1);
                        now.setDate(scope.inputs.day = d.getDate());
                        ngModelController.$setViewValue(now);
                    }
                }
                scope.checkForLeadingZero();
            };
            scope.popupDom = null;
            scope.inited = function () {
                var dom = angular.element(document.querySelectorAll("div.ionic-datepicker"));
                dom = dom[dom.length - 1];

                scope.popupDom = dom;
            };

            ngModelController.$render = function () {
                scope.inputs.day = ngModelController.$viewValue.getDate();
                scope.inputs.month = ngModelController.$viewValue.getMonth() + 1;
                scope.inputs.year = ngModelController.$viewValue.getFullYear();

                scope.checkForLeadingZero();
            };
            element.on('click', function () {
                var popup;
                scope.popup = popup = $ionicPopup.show({
                    title: scope.title,
                    //templateUrl: currentScriptPath.substring(0, currentScriptPath.lastIndexOf('/') + 1) + "template.html",
                    templateUrl: "ionic-datepicker/template.html",
                    scope: scope,
                    buttons: [
                        {
                            text: scope.setText,
                            type: "button-positive",
                            onTap: function (e) {
                                popup.close();
                                e.preventDefault();
                            }
                        }
                    ]
                });
            });
        }
    };
}]);